<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\web\View;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Books';
?>
<div class="book-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= Html::a('Create Book', '#', ['class'=>'btn btn-success', 'data-url' => Url::to(['books/create']), 'data-toggle' => 'modal', 'data-target' => '#modal']) ?>
    <?= Html::a('Create Author', '#', ['class'=>'btn btn-success', 'data-url' => Url::to(['authors/create']), 'data-toggle' => 'modal', 'data-target' => '#modal']) ?>
    <?= Html::a('Create Publisher', '#', ['class'=>'btn btn-success', 'data-url' => Url::to(['publishers/create']), 'data-toggle' => 'modal', 'data-target' => '#modal']) ?>

    <?php Pjax::begin(['id' => 'books-pjax', 'timeout' => 5000,'linkSelector' => 'a[data-pjax=1], th > a']); ?>
    <?= GridView::widget([
        'id' => 'books-pjax',
        'dataProvider' => $dataProvider,
		'layout'=>"<br/>{pager}<br/>{summary}<br/>{items}",
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => ['style' => 'width:50px']
            ],
            [
                'format' => 'html',
                'label' => 'Title',
                'value' => function ($data) {
                    return Html::a($data->title, ['books/update', 'id' => $data->id]) . '<br>' . Html::img($data->imageUrl, ['style' => 'width: 60px;']);
                },
            ],
            [
                'label' => 'Author',
                'attribute' => 'author.name',
            ],
            [
                'label' => 'Publisher',
                'attribute' => 'publisher.name',
            ],
            [
                'attribute' => 'published_at',
                'headerOptions' => ['style' => 'width:110px']
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{upd} {delete}',
                'buttons'=>[
                    'upd'=>function ($url, $data) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', ['data-url' => Url::to(['books/update', 'id' => $data->id]), 'data-toggle' => 'modal', 'data-target' => '#modal']);    
                    },
                   'delete'=>function ($url, $data) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', '#', ['data-url' => Url::to(['books/delete', 'id' => $data->id]),'class'=>'ajaxDelete']);    
                    }
                ],
                'headerOptions' => ['style' => 'width:50px']
            ],
        ],
    ]); ?>
<?php 
$this->registerJs("
    var modal = $('#modal');
    modal.modalForm();
    modal.on('ModalSubmit', function (event, data, status, xhr) {
        var contentType = xhr.getResponseHeader('content-type') || '';
        if (contentType.indexOf('json') > -1) {
            if (data.success){
                jQuery(this).modal('hide');
                jQuery.pjax({
                    push : true,
                    replace : false,
                    timeout : 5000,
                    scrollTo : false,
                    container : '#books-pjax'
                });
            }
        }
    });
    $('.ajaxDelete').on('click', function(e) {
        e.preventDefault();
        if (!confirm('Are you sure you want to delete this item?'))
            return;
        var deleteUrl = $(this).data('url');
        var pjaxContainer = 'books-pjax';
        $.ajax({
                url: deleteUrl,
                type: 'post',
                dataType: 'json',
                error: function(xhr, status, error) {
                    alert('There was an error with your request.' + xhr.responseText);
                }
        }).done(function(data) {
                $.pjax.reload({container: '#'+pjaxContainer });
        });                     
    });
", View::POS_READY);
Pjax::end(); ?>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

