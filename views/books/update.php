<?php

/* @var $this yii\web\View */
/* @var $model app\models\Book */
/* @var $upload app\models\UploadForm */
$this->title = 'Update Book: ' . $model->title;
?>
<div class="book-update">
    <?= $this->render('_form', [
        'model' => $model,
        'upload' => $upload,
    ]) ?>
</div>
