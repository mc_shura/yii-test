<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Author;
use app\models\Publisher;
/* @var $this yii\web\View */
/* @var $model app\models\Book */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="book-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?=
    $form->field($model, 'author_id')
        ->dropDownList(
            ArrayHelper::map(Author::find()->asArray()->all(), 'id', 'name')
        )
    ?>
    <?=
    $form->field($model, 'publisher_id')
        ->dropDownList(
            ArrayHelper::map(Publisher::find()->asArray()->all(), 'id', 'name')
        )
    ?>
    <?= $form->field($model, 'published_at')->textInput() ?>
    <?= $form->field($upload, 'imageFile')->fileInput() ?>
    <?php if (!$model->isNewRecord) echo Html::img($model->getImageUrl(), ['style'=>'width: 150px; margin-bottom:15px']); ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>