<?php

/* @var $this yii\web\View */
/* @var $model app\models\Publisher */

$this->title = 'Update Publisher: ' . $model->name;
?>
<?= $this->render('_form', [
    'model' => $model,
]) ?>
