<?php

/* @var $this yii\web\View */
/* @var $model app\models\Publisher */

$this->title = 'Create Publisher';
?>
<?= $this->render('_form', [
    'model' => $model,
]) ?>