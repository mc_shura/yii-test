<?php

/* @var $this yii\web\View */
/* @var $model app\models\Author */

$this->title = 'Update Author: ' . $model->name;
?>
<?= $this->render('_form', [
    'model' => $model,
]) ?>