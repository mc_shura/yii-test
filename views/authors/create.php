<?php

/* @var $this yii\web\View */
/* @var $model app\models\Author */

$this->title = 'Create Author';
?>
<?= $this->render('_form', [
    'model' => $model,
]) ?>