<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
/**
 * This is the model class for table "{{%books}}".
 *
 * @property string $id
 * @property string $title
 * @property string $image
 * @property string $author_id
 * @property string $publisher_id
 * @property string $published_at
 *
 * @property Author $author
 * @property Publisher $publisher
 */
class Book extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%books}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'author_id', 'publisher_id'], 'required'],
            [['author_id', 'publisher_id'], 'integer'],
            [['published_at'], 'default', 'value' => 1970],
            [['published_at'], 'date', 'format'=>'yyyy'],
            [['title', 'image'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Author::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['publisher_id'], 'exist', 'skipOnError' => true, 'targetClass' => Publisher::className(), 'targetAttribute' => ['publisher_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'image' => 'Image',
            'imageFile' => 'Image',
            'author_id' => 'Author ID',
            'publisher_id' => 'Publisher ID',
            'published_at' => 'Published At',
        ];
    }
	
    /**
     * Delete image with model
     * @return bool
     */
    public function beforeDelete()
    {

        if (parent::beforeDelete()) {
			$file = Yii::getAlias('@webroot'). '/upload/images/' . $this->image;
            if ($this->image && file_exists($file))
                unlink($file);
            return true;
        } else {
            return false;
        }
    }
    /**
     * @return string Image Url
     */
    public function getImageUrl()
    {
        $file = Yii::getAlias('@webroot'). '/upload/images/' . $this->image;

        if (!$this->image || !is_file($file))
            $this->image = 'default.jpg';
        return Url::to('@web/upload/images/' . $this->image, true);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Author::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublisher()
    {
        return $this->hasOne(Publisher::className(), ['id' => 'publisher_id']);
    }
}
