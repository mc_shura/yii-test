<?php

namespace app\controllers;

use Yii;
use app\models\Book;
use app\models\UploadForm;
use app\models\BookSearch;
use yii\web\Response;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * BookController implements the CRUD actions for Book model.
 */
class BooksController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Book models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BookSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Book model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Book model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Book();
        $upload = new UploadForm();
        if ($model->load(Yii::$app->request->post())) {
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            if ($upload->validate()) {
                $file = Yii::getAlias('@webroot'). '/upload/images/' . md5(time()) . '.' . $upload->imageFile->extension;
                if ($upload->imageFile){
                    $upload->imageFile->saveAs($file);
                    $model->image = basename($file);
                }
                if ($model->save()) {
                    if (Yii::$app->request->isAjax) {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return ['success' => true];
                    }
                    return $this->redirect('/');
                } else {
                    if (Yii::$app->request->isAjax) {
                        return $this->renderAjax('_form-ajax', [
                            'model' => $model,
                            'upload' => $upload,
                        ]);
                    } else {
                        return $this->render('update', [
                            'model' => $model,
                            'upload' => $upload,
                        ]);
                    }
                }
            }
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('_form-ajax', [
                    'model' => $model,
                    'upload' => $upload,
                ]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'upload' => $upload,
                ]);
            }
        }
    }

    /**
     * Updates an existing Book model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $upload = new UploadForm();
        if ($model->load(Yii::$app->request->post())) {
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            
            if ($upload->validate()) {
                $file = Yii::getAlias('@webroot'). '/upload/images/' . md5(time()) . '.' . $upload->imageFile->extension;
                if ($upload->imageFile){
					$oldFile = Yii::getAlias('@webroot'). '/upload/images/' . $model->image;
					if ($model->image && file_exists($oldFile))
						unlink($oldFile);
                    $upload->imageFile->saveAs($file);
                    $model->image = basename($file);
                }
                if($model->save()){
                
                    if (Yii::$app->request->isAjax) {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return ['success' => true];
                    }
                    return $this->redirect('/');
                } else {
                    if (Yii::$app->request->isAjax) {
                        return $this->renderAjax('_form-ajax', [
                            'model' => $model,
                            'upload' => $upload,
                        ]);
                    } else {
                        return $this->render('update', [
                            'model' => $model,
                            'upload' => $upload,
                        ]);
                    }
                }
            }
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('_form-ajax', [
                    'model' => $model,
                    'upload' => $upload,
                ]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'upload' => $upload,
                ]);
            }
        }
    }

    /**
     * Deletes an existing Book model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['success' => true];
        }
        return $this->redirect(['/']);
    }

    /**
     * Finds the Book model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Book the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Book::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
