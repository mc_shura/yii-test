-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.50 - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных book_test
CREATE DATABASE IF NOT EXISTS `book_test` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `book_test`;


-- Дамп структуры для таблица book_test.t_authors
CREATE TABLE IF NOT EXISTS `t_authors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы book_test.t_authors: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `t_authors` DISABLE KEYS */;
INSERT INTO `t_authors` (`id`, `name`) VALUES
	(1, 'Джоан Роулинг');
/*!40000 ALTER TABLE `t_authors` ENABLE KEYS */;


-- Дамп структуры для таблица book_test.t_books
CREATE TABLE IF NOT EXISTS `t_books` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `publisher_id` int(10) unsigned NOT NULL,
  `published_at` year(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_t_books_t_authors` (`author_id`),
  KEY `FK_t_books_t_publishers` (`publisher_id`),
  CONSTRAINT `FK_t_books_t_authors` FOREIGN KEY (`author_id`) REFERENCES `t_authors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_t_books_t_publishers` FOREIGN KEY (`publisher_id`) REFERENCES `t_publishers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы book_test.t_books: ~7 rows (приблизительно)
/*!40000 ALTER TABLE `t_books` DISABLE KEYS */;
INSERT INTO `t_books` (`id`, `title`, `image`, `author_id`, `publisher_id`, `published_at`) VALUES
	(2, 'Гарри Поттер и философский камень', 'D._K._Rouling__Garri_Potter_i_filosofskij_kamen.jpg', 1, 1, '1997'),
	(3, 'Гарри Поттер и Тайная комната', '', 1, 1, '1998'),
	(4, 'Гарри Поттер и узник Азкабана', '', 1, 1, '1999'),
	(5, 'Гарри Поттер и Кубок Огня', '', 1, 1, '2000'),
	(6, 'Гарри Поттер и Орден Феникса', '', 1, 1, '2003'),
	(7, 'Гарри Поттер и Принц-полукровка', '', 1, 1, '2005'),
	(8, 'Гарри Поттер и Дары Смерти', '', 1, 1, '2007');
/*!40000 ALTER TABLE `t_books` ENABLE KEYS */;


-- Дамп структуры для таблица book_test.t_publishers
CREATE TABLE IF NOT EXISTS `t_publishers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы book_test.t_publishers: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `t_publishers` DISABLE KEYS */;
INSERT INTO `t_publishers` (`id`, `name`) VALUES
	(1, 'Махаон'),
	(2, 'Издательство 2'),
	(3, 'Махаон 2'),
	(4, 'Махаон 3');
/*!40000 ALTER TABLE `t_publishers` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
