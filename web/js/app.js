/**
 * Created by Admin on 03.03.2017.
 */

(function($) {
    "use strict";

    function ModalForm(element, options) {
        this.element = element;
        this.init(options);
    }

    ModalForm.prototype.init = function() {
        $(this.element).on('show.bs.modal', this.shown.bind(this));
    };

    ModalForm.prototype.shown = function(event) {
        var url = $(event.relatedTarget).data('url');
        $(this.element).find('.modal-content').html('');
        $.ajax({
            url: url,
            context: this,
            beforeSend: function (xhr, settings) {
                $(this.element).triggerHandler('ModalBeforeShow', [xhr, settings]);
            },
            success: function(data, status, xhr) {
                $(this.element).find('.modal-content').html(data);
                $(this.element).off('submit').on('submit', this.formSubmit.bind(this));
                $(this.element).triggerHandler('ModalShow', [data, status, xhr]);
            }
        });
    };

    ModalForm.prototype.formSubmit = function() {
        var form = $(this.element).find('form');
        var formData = new FormData(form[0]);
        $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: formData,
            processData: false,
            contentType: false,
            context: this,
            beforeSend: function (xhr, settings) {
                $(this.element).triggerHandler('ModalBeforeSubmit', [xhr, settings]);
            },
            success: function(data, status, xhr) {
                var contentType = xhr.getResponseHeader('content-type') || '';
                if (contentType.indexOf('html') > -1) {
                    $(this.element).find('.modal-content').html(data);
                }
                $(this.element).triggerHandler('ModalSubmit', [data, status, xhr]);
            }
        });
        return false;
    };

    $.fn.modalForm = function(options) {
        return this.each(function () {
            if (!$.data(this, 'modalForm')) {
                $.data(this, 'modalForm', new ModalForm(this, options));
            }
        });
    };
})(jQuery);